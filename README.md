# gwcloud_job_server_helm

Helm chart repository for gwcloud_job_server
# gwcloud_job_server_helm

Helm chart repository for gwcloud_job_server

## Assumptions
- Helm3 is [installed](https://helm.sh/docs/intro/install/)
- Relevant docker images and container image registry exists
- Relevant vault access and secrets exists
- Relevant storage access and storage solutions exist

## Repo Initialisation
- Initialise helm repo by executing `helm create gwcloud_job_server`
- Move helm output by `mv gwcloud_job_server/* . && mv gwcloud_job_server/.helmignore .`
- Cleanup `rmdir gwcloud_job_server`

## Chart values
Dynamic variables declared and initialised in [values.yaml](./values.yaml)
| Variable | Default | Comment |
| --- | --- | --- |
| `deployment.name` | `gwcloud-job-server` | Name value of k8s deployment resource |
| `image.repository` | `nexus.gwdc.org.au/docker/gwcloud_job_server` | Container image repository |
| `image.tag` | `""` |  Main chart application `gwcloud-job-server` image tag controlled through `appVersion` in [Chart.yaml](./Chart.yaml) |
| `vaultAnnotations.role` | `gwcloud-job-server` | Role configured at the vault server `vault.gwdc.org.au` |
| `vaultAnnotations.secrets` | [] | list of vault kv engines used as a reference for populating container env variables |
|||

## Architecture
TBA


## Prerequisites
```bash
# Vault dependencies
#   Vault cli required
#   cli must have network access to vault
vault login -address=$VAULT_HTTPS_FQDN -method=github -token=$ACCESS_TOKEN

# Add vault policy
tee job-server.policy.hcl <<EOF
# Read job-server deployment config
path "kv/gwcloud/job-server"
{
  capabilities = ["list", "read"]
}

# Read gwcloud common credentials
path "kv/gwcloud/common"
{
  capabilities = ["list", "read"]
}
EOF
vault policy write job-server job-server.policy.hcl

# Sanity Check
vault policy read job-server

# Add vault kubernetes role
vault write job-server/kubernetes/role/job-server \
    bound_service_account_names=gwcloud-job-server \
    bound_service_account_namespaces=gwcloud \
    policies=default,job-server \
    ttl=1h

# Sanity Check
vault read job-server/kubernetes/role/job-server
```

## Support
TBA

## ToDo
- [x] Integrate Vault secrets.
- [x] Proof of concept deployment.
- [x] CI for testing the helm chart.
- [x] Make port definitions dynamic.
- [ ] CD for deploying packaged charts to `https://nexus.gwdc.org.au/#browse/browse:helm`
- [ ] CD Runtime test to dev k8s cluster.
