.PHONY: lint package render

lint:
	helm lint

package: lint
	rm -rf *.tgz
	helm package $(PWD)

upload: package
	curl -u "$(_HELM_USR):$(_HELM_PSW)" https://nexus.gwdc.org.au/repository/helm/ --upload-file `ls *.tgz` -v

values:
	@helm show values .

render:
	@helm template .
